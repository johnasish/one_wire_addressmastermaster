/////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////
#include "Read_Write.h"
#include "gpio_driver.h"
#include <stdio.h>
#include "intrinsics.h"
/////////////////////////dsb reset////////////////////////////////
uint8_t one_Wire_Reset()//Reset_oneWire
{
        GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
	GPIO_WriteLow(GPIO3,GPIO_PIN_0);
	delay_us(RESET_LOW_Dly);//for 480us

	GPIO_WriteHigh(GPIO3,GPIO_PIN_0);

	delay_us(RESET_HIGH_Dly);//for70 us
	if(GPIO_ReadInputPin(GPIO3,GPIO_PIN_0) ==0)
	{
		delay_us(RESET_SAMPLE_Dly);//for 410 us
		return 1;
	}
        else
        {
        delay_us(RESET_SAMPLE_Dly);//for 410 us
	return 0;
        }

}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void one_wire_Write_bit(uint8_t bit)
{
    if(bit)
  {
    GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
    GPIO_WriteLow(GPIO3,GPIO_PIN_0);
    asm("nop");//for 2 us
    GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
    delay_us(WRITE1_HIGH_Dly);//for 64 us
  }
  else
  {
    GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
    GPIO_WriteLow(GPIO3,GPIO_PIN_0);
    delay_us(WRITE0_LOW_Dly);////for 60 usec 
    GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
    delay_us(WRITE0_HIGH_Dly);////for 10 usec
    
  }
}
////////////////////////////////////////////////////////////////////////////////
 uint8_t one_wire_Read_bit(void)
{
  uint8_t resp;
  GPIO_WriteLow(GPIO3,GPIO_PIN_0);
  asm("nop");//for 2 us
  //delay_us(3);////for 3 usec = 65/.7501(time for one increment)
  GPIO_WriteHigh(GPIO3,GPIO_PIN_0);
  delay_us(M_READ_LOW_Dly);//for 10 us
  //delay_us(10);////16 for 12 usec = 65/.7501(time for one increment)
  resp=GPIO_ReadInputPin(GPIO3,GPIO_PIN_0);
  //delay_us(53);////for 49 usec = 65/.7501(time for one increment)
  delay_us(M_READ_HIGH_Dly);//for 53 us
  return resp;
}
////////////////////////////////////////////////////////////////////////////////
void one_wire_Write_byte(uint8_t byte)
{
   	uint8_t i;
	for(i=0;i<8;i++)
	{
		one_wire_Write_bit(byte & 0x01);
		byte>>=1;
	}
}
////////////////////////////////////////////////////////////////////////////////
uint8_t one_wire_Read_byte(void)
{
  uint8_t i,Result=0;
  for(i=0;i<8;i++)
  {
    if(one_wire_Read_bit())
    {
      Result=Result | 1<<i;
    }
  }
  return Result;
}
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void delay_us(int us)
{
  int i=0;
  for(i=0;i<us;i++)
          {
            asm("nop");
          }
}
//////////////////////////////////////////////////////////////////////////////////
void delay_ms(int ms)
{
  int j=0,k=0;
  for(j=0;j<ms;j++)
  {
    for(k=0;k<ms;k++)
    {
      
    }
  }
 
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////