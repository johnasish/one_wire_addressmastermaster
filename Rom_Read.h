#ifndef Rom_Read
#define Rom_Read
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//<<<<<<<<<<<^^^^^^ ROM COMMANDS ^^^^^>>>>>>>>>>//
#define Search_rom 		 0xF0 
#define Read_rom 		 0x33
#define Match_rom 		 0x55
#define Skip_rom 		 0xCC
#define Alarm_search 	         0xEC

//<<<<<<<<<<<^^^^^^ DS18B20 COMMANDS ^^^^^>>>>>>>>>>//
#define Temp_convert   0x44
#define Write_scr_pad  0x4E
#define Read_scr_pad   0xBE
#define Copy_scr_pad   0x48
#define Recall         0xB8
//----------------------------------------------------------------------------------//
///////////////////////////////////////////////////////////////////////////////
#include "gpio_driver.h"
///////////////////////////////////////////////////////////////////////////////
//typedef union 
//{
//	uint32_t BYTE;
//	unsigned char data[8];
//}save;
//struct dat
//{
//  uint8_t  full_add[8];
//  uint16_t celcius1;
//  float    fahrenheit1;
//  uint8_t  total_dev;
//  
//};
////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint8_t ID[8];
}ROMType;
////////////////////////////////////////////////////////////////////////////////
uint8_t Read_R(/*save**/);
void onewire_start_temperature_conversion();//uint8_t temp_con();
uint8_t Read_scratch();
//uint8_t Match_rm();
//uint8_t First();
//uint8_t Search(uint8_t);
//struct dat * find_dev();
//uint8_t deviceno();
//uint8_t crc8( uint8_t *data, uint16_t number_of_bytes_in_data );
//struct dat * pop_values();
////////////////////////////////////////////////////////////////////////////////
uint8_t First1(ROMType * pRombuffer1);
uint8_t Search1(ROMType * pRombuffer1);
uint8_t find_dev1(ROMType * pRombuffer1, uint8_t Rombufferszie1,uint8_t *pNumDevices);
void onewire_find_devices(ROMType *pRombuffer, uint8_t Rombufferszie, uint8_t *pNumDevices);
void onewire_get_temperature(ROMType *pRombuffer, uint8_t NumDevices, uint16_t *pTempbuffer);
uint8_t  Match_rm1(ROMType *pRombuffer, uint16_t *pTempbuffer);
////////////////////////////////////////////////////////////////////////////////






#endif