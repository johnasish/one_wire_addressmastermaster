#ifndef    Read_Write
#define    Read_Write
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//asm("nop")  = 2us....4v out
//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
//#define WRITE1_LOW_Dly		 3 single asm(without loop..for 2 us)  
#define	WRITE1_HIGH_Dly		 	 38   //64 us
#define	WRITE0_LOW_Dly		 	35    //60 us
#define	WRITE0_HIGH_Dly		 	 3    //10 us
#define	M_READ_LOW_Dly		 	 3    //10 us 
#define	M_READ_HIGH_Dly			 31   //31 us
//#define	INIT_Dly	         2 single asm(without loop..for 2 us)
#define	RESET_LOW_Dly			 300  //480 us
#define	RESET_HIGH_Dly			  42  //70  us
#define	RESET_SAMPLE_Dly		 258  //410 us
#define WAIT_Dly		          7   //15  us
#define T_CONV_Dly		        750 //ms //705 in ms loop
///////////////////////////////////////////////////////////////////////////////
#include "gpio_driver.h"
void delay_us(int u_Sec);
uint8_t one_Wire_Reset();
//void dsb_write_command(uint8_t);
void one_wire_Write_bit(uint8_t bit);
uint8_t one_wire_Read_bit(void);
void delay_us(int );
void delay_ms(int );
uint8_t one_wire_Read_byte(void);
void one_wire_Write_byte(uint8_t byte);
////////////////////////////////////////////////////////////////////////////////
#endif