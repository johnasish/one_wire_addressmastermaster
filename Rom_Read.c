///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
#include "Rom_Read.h"
#include "Read_Write.h"
#include "gpio_driver.h"
#include <stdio.h>
#include "calc.h"
///////////////////////////////////////////////////////////////////
#define CRC8INIT    0x00
#define CRC8POLY    0x18              //0X18 = X^8+X^5+X^4+X^0
#define FALSE 0
#define TRUE  1
////////////////////////////////////////////////////////////////////
uint8_t LastDiscrepancy,column;;
uint8_t LastFamilyDiscrepancy;
uint8_t LastDeviceFlag,ROM_NO[8],NumDevices;
uint8_t crc81,no_dev=0,total_dev=3;
extern uint8_t No_Devices,check_Rst;
////////////////////////////////////////////////////////////////////
uint8_t Rom_data[8];
uint16_t Temp_reg[9];
//////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
//uint8_t Read_R(/*save * str*/)
//{
//  uint8_t i;
//  one_wire_Write_byte(0x33);///code for READ ROM
//  for(i=0;i<8;i++)
//    {
//      Rom_data[i]=one_wire_Read_byte();
//     
//    }
//    return 1;
//}
///////////////////////////////////////////////////////////////////////
uint8_t crc8( uint8_t *data, uint16_t number_of_bytes_in_data )
{
	uint8_t  crc;
	uint16_t loop_count;
	uint8_t  bit_counter;
	uint8_t  b;
	uint8_t  feedback_bit;

	crc = CRC8INIT;

	for (loop_count = 0; loop_count != number_of_bytes_in_data; loop_count++)
	{
		b = data[loop_count];

		bit_counter = 8;
		do {
			feedback_bit = (crc ^ b) & 0x01;

			if ( feedback_bit == 0x01 ) {
				crc = crc ^ CRC8POLY;
			}
			crc = (crc >> 1) & 0x7F;
			if ( feedback_bit == 0x01 ) {
				crc = crc | 0x80;
			}

			b = b >> 1;
			bit_counter--;

		} while (bit_counter > 0);
	}

	return crc;
}
////////////////////////////////////////////////////////////////////////////////
void onewire_start_temperature_conversion()
{
 
  one_wire_Write_byte(0xcc);//for skip rom
  delay_us(3);//for 10 us delay
  one_wire_Write_byte(0x44);//for temp con initi
  delay_ms(T_CONV_Dly);//for 750 ms
  while(!GPIO_ReadInputPin(GPIO3,GPIO_PIN_0));
  //return 1;
}
////////////////////////////////////////////////////////////////////////////////
//uint8_t  Read_scratch()
//{
//  uint8_t M_com;
//  one_Wire_Reset();
//  M_com=Match_rm();
//  return 1;  
//}
///////////////////////////////////////////////////////////////////////////////
//uint8_t  Match_rm()
//{
//  uint8_t i,j,cal_com=0;
//  one_Wire_Reset();
//  one_wire_Write_byte(0x55);//match rom command
//  for(j=0 ; j<total_dev ; j++)////
//  {
//  for(i=0;i<8;i++)
//    {
//      
//      one_wire_Write_byte(s[j].full_add[i]);
//    }
//  one_wire_Write_byte(0xBE);//for Read scratch
//  delay_ms(2);
//  for(i=0;i<9;i++)
//    {
//      Temp_reg[i]=one_wire_Read_byte();
//    }
// 
//  delay_us(WAIT_Dly);//////
//  one_Wire_Reset();
//  one_wire_Write_byte(0x55);//match rom command
//  }
//  delay_us(WAIT_Dly);//for 15 usec
//  delay_us(RESET_SAMPLE_Dly);//for 410 usec
//  return 1;
//}
////////////////////////////////////////////////////////////////////////////////
//struct dat * pop_values()
//{
//  temporary_reg=Tem_grouped(s);
//  return temporary_reg;
//}
////////////////////////////////////////////////////////////////////////////////
//struct dat * find_dev()///
//{
//  
//  if(First())
//    {
//      do 
//        {
//          no_dev++;
//          if(no_dev >= No_Devices)
//            {
//              break;
//            }
//         } while (Search(no_dev));
//     }
//  return s;///
//}
//////////////////////////to find the first device /////////////////////////////
//uint8_t First()
//{
//   one_Wire_Reset();// reset the search state
//   LastDiscrepancy = 0;
//   LastDeviceFlag = FALSE;
//   LastFamilyDiscrepancy = 0;
//
//   return Search(no_dev);
//}
////////////////////////////////////////////////////////////////////////////////
//uint8_t deviceno()
//{
//  return st_count+1;
//}
////////////////////////search fun//////////////////////////////////////////////
//uint8_t Search(uint8_t row)
//{
//   int id_bit_number;
//   int last_zero, rom_byte_number, search_result;
//   int id_bit, cmp_id_bit;
//   unsigned char rom_byte_mask, search_direction,total_dev;
//
//   // initialize for search
//   id_bit_number = 1;
//   last_zero = 0;
//   rom_byte_number = 0;
//   rom_byte_mask = 1;
//   search_result = 0;
//   crc81 = 0;
//   column=0;
//////////////////////////////////////////////////////////////////////////////////
//    if (!LastDeviceFlag)
//      {
//        if (! one_Wire_Reset())
//          {
//             // reset the search
//             LastDiscrepancy = 0;
//             LastDeviceFlag = FALSE;
//             LastFamilyDiscrepancy = 0;
//             return FALSE;
//           }
//        one_wire_Write_byte(0xF0);/////search rom command
//        do
//          {
//              id_bit = one_wire_Read_bit();
//              cmp_id_bit = one_wire_Read_bit();
//              if ((id_bit == 1) && (cmp_id_bit == 1))
//                break;
//              else
//                {
//                  if(id_bit != cmp_id_bit)////device coupled 0 or 1
//                      search_direction = id_bit;//write val for search 
//                  else
//                    {
//                      // if this discrepancy if before the Last Discrepancy
//                      // on a previous next then pick the same as last time
//                      if (id_bit_number < LastDiscrepancy)
//                            search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
//                      else
//                        // if equal to last pick 1, if not then pick 0
//                        search_direction = (id_bit_number == LastDiscrepancy);
//                      // if 0 was picked then record its position in LastZero
//                      if (search_direction == 0)
//                        {
//                          last_zero = id_bit_number;
//                          // check for Last discrepancy in family
//                          if (last_zero < 9)
//                            LastFamilyDiscrepancy = last_zero;
//                         }
//                      }
//                      // set or clear the bit in the ROM byte rom_byte_number
//                      // with mask rom_byte_mask
//                      if (search_direction == 1)
//                        {
//                          
//                        ROM_NO[rom_byte_number] |= rom_byte_mask;
//                        //ROM_ID[row][column]= ROM_NO[rom_byte_number];
//                        s[st_count].full_add[rom_byte_number]|=rom_byte_mask ;
//                          
//                        }
//                      else
//                        {
//                        ROM_NO[rom_byte_number] &= ~rom_byte_mask;
//                        //ROM_ID[row][column]= ROM_NO[rom_byte_number];
//                        s[st_count].full_add[rom_byte_number]&=~rom_byte_mask ;
//                        
//                        }
//                      // serial number search direction write bit
//                      one_wire_Write_bit(search_direction);
//
//                      // increment the byte counter id_bit_number
//                      // and shift the mask rom_byte_mask
//                      id_bit_number++;
//                      column++;
//                      rom_byte_mask <<= 1;
//
//                      // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
//                      if (rom_byte_mask == 0)
//                      {
//                          //docrc8(ROM_NO[rom_byte_number]);  // accumulate the CRC
//                          rom_byte_number++;
//                          rom_byte_mask = 1;
//                      }
//                }
//          }while(rom_byte_number < 8);
//        st_count++;
//        rom_byte_number=0;
//        crc81 = crc8(ROM_NO , 8 );
//         // if the search was successful then
//      if (!((id_bit_number < 65) || (crc81 != 0)))
//      {
//         // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
//         LastDiscrepancy = last_zero;
//
//         // check for last device
//         if (LastDiscrepancy == 0)
//            LastDeviceFlag = TRUE;
//         
//         search_result = TRUE;
//      }
//   }
//    // if no device found then reset counters so next 'search' will be like a first
//   if (!search_result || !ROM_NO[0])
//   {
//      LastDiscrepancy = 0;
//      LastDeviceFlag = FALSE;
//      LastFamilyDiscrepancy = 0;
//      search_result = FALSE;
//   }
//      return search_result;
//}                      
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void onewire_find_devices(ROMType *pRombuffer, uint8_t Rombufferszie, uint8_t *pNumDevices)
{
  check_Rst = one_Wire_Reset();
   if(check_Rst == 1)
        { 
          if(find_dev1(pRombuffer ,Rombufferszie, pNumDevices))
            {   
              
              one_wire_Write_byte(0xcc);//for skip rom
            }
           else
            {
            }

        }
}
////////////////////////////////////////////////////////////////////////////////
uint8_t find_dev1(ROMType *pRombuffer1,uint8_t Rombufferszie1,uint8_t *pNumDevices)///
{
  No_Devices=Rombufferszie1;
  if((First1(pRombuffer1)))
    {
      do 
        {
          NumDevices=no_dev;
          no_dev++;
          if(no_dev >= No_Devices)
            {
              break;
            }
          pRombuffer1++;
         } while (Search1(pRombuffer1));
       *pNumDevices=no_dev;
     }
  return 1;
}
////////////////////////////////////////////////////////////////////////////////
uint8_t First1(ROMType * pRombuffer1)
{
   one_Wire_Reset();// reset the search state
   LastDiscrepancy = 0;
   LastDeviceFlag = FALSE;
   LastFamilyDiscrepancy = 0;
   return Search1(pRombuffer1);
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////search fun//////////////////////////////////////////////
uint8_t Search1(ROMType * pRombuffer1)
{
   int id_bit_number;
   int last_zero, rom_byte_number, search_result;
   int id_bit, cmp_id_bit;
   unsigned char rom_byte_mask, search_direction,total_dev;

   // initialize for search
   id_bit_number = 1;
   last_zero = 0;
   rom_byte_number = 0;
   rom_byte_mask = 1;
   search_result = 0;
   crc81 = 0;
   column=0;
////////////////////////////////////////////////////////////////////////////////
    if (!LastDeviceFlag)
      {
        if (! one_Wire_Reset())
          {
             // reset the search
             LastDiscrepancy = 0;
             LastDeviceFlag = FALSE;
             LastFamilyDiscrepancy = 0;
             return FALSE;
           }
        one_wire_Write_byte(0xF0);/////search rom command
        do
          {
              id_bit = one_wire_Read_bit();
              cmp_id_bit = one_wire_Read_bit();
              if ((id_bit == 1) && (cmp_id_bit == 1))
                break;
              else
                {
                  if(id_bit != cmp_id_bit)////device coupled 0 or 1
                      search_direction = id_bit;//write val for search 
                  else
                    {
                      // if this discrepancy if before the Last Discrepancy
                      // on a previous next then pick the same as last time
                      if (id_bit_number < LastDiscrepancy)
                            //search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
                           search_direction = ((ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
                      else
                        // if equal to last pick 1, if not then pick 0
                        search_direction = (id_bit_number == LastDiscrepancy);
                      // if 0 was picked then record its position in LastZero
                      if (search_direction == 0)
                        {
                          last_zero = id_bit_number;
                          // check for Last discrepancy in family
                          if (last_zero < 9)
                            LastFamilyDiscrepancy = last_zero;
                         }
                      }
                      // set or clear the bit in the ROM byte rom_byte_number
                      // with mask rom_byte_mask
                      if (search_direction == 1)
                        {
                          
                       ROM_NO[rom_byte_number] |= rom_byte_mask;
                        ((*pRombuffer1).ID[rom_byte_number]) |= rom_byte_mask;
                        //ROM_ID[row][column]= ROM_NO[rom_byte_number];
                       // s[st_count].full_add[rom_byte_number]|=rom_byte_mask ;
                          
                        }
                      else
                        {
                        ROM_NO[rom_byte_number] &= ~rom_byte_mask;
                        ((*pRombuffer1).ID[rom_byte_number]) &=~ rom_byte_mask;
                        //ROM_ID[row][column]= ROM_NO[rom_byte_number];
                       // s[st_count].full_add[rom_byte_number]&=~rom_byte_mask ;
                        
                        }
                      // serial number search direction write bit
                      one_wire_Write_bit(search_direction);

                      // increment the byte counter id_bit_number
                      // and shift the mask rom_byte_mask
                      id_bit_number++;
                      column++;
                      rom_byte_mask <<= 1;

                      // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
                      if (rom_byte_mask == 0)
                      {
                          //docrc8(ROM_NO[rom_byte_number]);  // accumulate the CRC
                          rom_byte_number++;
                          rom_byte_mask = 1;
                      }
                }
          }while(rom_byte_number < 8);
        //st_count++;
        rom_byte_number=0;
        crc81 = crc8(ROM_NO , 8 );
         // if the search was successful then
      if (!((id_bit_number < 65) || (crc81 != 0)))
      {
         // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
         LastDiscrepancy = last_zero;

         // check for last device
         if (LastDiscrepancy == 0)
            LastDeviceFlag = TRUE;
         
         search_result = TRUE;
      }
   }
    // if no device found then reset counters so next 'search' will be like a first
   if (!search_result || !ROM_NO[0])
   {
      LastDiscrepancy = 0;
      LastDeviceFlag = FALSE;
      LastFamilyDiscrepancy = 0;
      search_result = FALSE;
   }
      return search_result;
}
////////////////////////////////////////////////////////////////////////////////
void onewire_get_temperature(ROMType *pRombuffer, uint8_t NumDevices, uint16_t *pTempbuffer)
{
   uint8_t M_com;
   one_Wire_Reset();
   Match_rm1(pRombuffer,pTempbuffer);
   delay_ms(1000); 
}
////////////////////////////////////////////////////////////////////////////////
uint8_t  Match_rm1(ROMType *pRombuffer, uint16_t *pTempbuffer)
{
  uint8_t i,j;
  //one_Wire_Reset();
  one_wire_Write_byte(0x55);//match rom command
  for(j=0 ; j<no_dev ; j++)////
  {
  for(i=0;i<8;i++)
    {
      //Write_byte(Rom_data[i]);//only for single device read
       // Write_byte(ROM_ID[row_no][i]);
      //one_wire_Write_byte(s[j].full_add[i]);
      one_wire_Write_byte((*(pRombuffer+j)).ID[i]);
    }
  one_wire_Write_byte(0xBE);//for Read scratch
  delay_ms(2);
  for(i=0;i<9;i++)
    {
      Temp_reg[i]=one_wire_Read_byte();
    }
  Temp_calc(j,pTempbuffer);
  delay_us(WAIT_Dly);//////
  one_Wire_Reset();
  one_wire_Write_byte(0x55);//match rom command
  }
  delay_us(WAIT_Dly);//for 15 usec
  delay_us(RESET_SAMPLE_Dly);//for 410 usec
  return 1;
}